import java.awt.Font;
import java.awt.Graphics;
import java.awt.Color;

import javax.swing.JPanel;

/**
 * This class contains all visual components' instructions in a inanimate state.
 * @author Ivan J. Miranda
 *
 */

@SuppressWarnings("serial")
public class Interface extends JPanel
{
	/*All size values are ratios corresponding to window's size as a function. This is made this way in order to easily convert the 
	 *interactive window into a resizable window if needed in the future. Because of that, I've declared location and size variables type double.
	 *They are casted to type int at the moment that any method requires it. Also some objects need double precision in their instant ratios,
	 *like the fuel tank. Objects that do not store data other than visual properties of an instant are declared and initialized in the 
	 *paint method and do not contain set or get methods (just like most drawing methods). Objects that store data over time, like the revolutions 
	 *counter and the rectangular button (button state), are initialized outside the paint method and are updated via mutator methods created for the 
	 *particular object.*/
	
	//Font and color global properties.
	public static Color backgroundColor = Color.BLACK;
	public static Color drawingColor = Color.WHITE;
	public static Color buttonsColor = Color.GRAY;
	public static int headerLetterSize = (int)(MainLauncher.controllerWidth/37.5);	
	
	//Revolutions counter initial visual properties. 
	public static final int REVOLUTIONS_COUNTER_POSITIONS = 7;
	public static double revolutionsCounterX = (MainLauncher.controllerWidth/3.0) - 15;
	public static double revolutionsCounterY = (MainLauncher.controllerWidth/22.5);
	public static double revolutionsCounterWidth = (MainLauncher.controllerWidth/3.0);
	public static double revolutionsCounterHeight = (MainLauncher.controllerWidth/15.0);
	public static int revolutionsCounterDigitsSize = (int)(MainLauncher.controllerWidth/12.5);
	public static RevolutionsCounter counter = new RevolutionsCounter((int)revolutionsCounterX, (int)revolutionsCounterY, 
			(int)revolutionsCounterWidth, (int)revolutionsCounterHeight, 
			REVOLUTIONS_COUNTER_POSITIONS, revolutionsCounterDigitsSize
			);
	
	//Wheel initial visual properties.
	public static final char[] WHEEL_HEADER_CHARACTER = {'W', 'H', 'E', 'E', 'L'};
	public static double wheelX = MainLauncher.controllerWidth/2.4;
	public static double wheelY = MainLauncher.controllerWidth/6.0;
	public static double wheelDiameter = MainLauncher.controllerWidth/3.0;
	public static double wheelRadius = wheelDiameter/2;
	public static double startingAngle = 270; 
	public static double wheelAngle = startingAngle; //This one is double to maintain precision while rotating and changing speeds.
	CarWheel wheel; 
	
	//Speed-o-meter initial visual properties.
	public static final char[] SPEEDOMETER_HEADER_CHARACTER = {'S', 'P', 'E', 'E', 'D', 'O', 'M', 'E', 'T','E', 'R'};
	public static final int SPEEDOMETER_NUMBER_OF_MARKS = 8;
	public static double speedometerX = MainLauncher.controllerWidth/20.0;
	public static double speedometerY = MainLauncher.controllerWidth/6.0;
	public static double speedometerDiameter = MainLauncher.controllerWidth/3.0;
	public static double speedometerRadius = (speedometerDiameter)/2.0; 	
	public static int speedometerDigitsSize = (int)(MainLauncher.controllerWidth/37.5);
	public static double initialPointerPosition = (3*360.0/SPEEDOMETER_NUMBER_OF_MARKS);
	public static Color pointerColor = Color.RED;
	public static Color speedControllerIndicatorColor = Color.GRAY;
	public static double speedControllerX = wheelX + 5;
	public static double speedControllerY = wheelY + wheelDiameter + 20;
	public static double speedControllerWidth = wheelDiameter;
	public static double speedControllerHeight = wheelDiameter/8.5;
	public static SpeedOMeter speedOMeter = new SpeedOMeter((int)(speedometerX), (int)(speedometerY), 
													(int)(speedometerRadius), SPEEDOMETER_NUMBER_OF_MARKS, speedometerDigitsSize
													);
	public static RectangularButton speedUp = new RectangularButton((int)speedControllerX, (int)(speedControllerY + speedControllerHeight + 40),
																	(int)(speedControllerWidth/4.0), (int)speedControllerHeight
																	);
	public static RectangularButton speedDown = new RectangularButton((int)speedControllerX, (int)(speedControllerY + speedControllerHeight + 80),
																	  (int)(speedControllerWidth/4.0), (int)speedControllerHeight
			                                                          );
	CarGaugePointer speedOMeterPointer;
	
	//Fuel tank's and fuel meter initial visual properties.
	public static final char[] FUEL_TANK_HEADER_CHARACTER = {'F', 'U', 'E', 'L', ' ', 'T', 'A', 'N', 'K'};
	public static double tankX = MainLauncher.controllerWidth/1.175;
	public static double tankY = MainLauncher.controllerWidth/7.0;
	public static double tankWidth = MainLauncher.controllerWidth/12.5;
	public static double tankHeight = MainLauncher.controllerWidth/2.5;
	public static int tankCapacity = 60; //TODO *CHANGE HERE TO CHANGE TANK CAPACITY IF YOU WANT TO TRY OTHER POSSIBILITIES! WORKS FOR ALL! EXCEPT 0... :D*
	public static int tankActualLoad = 0;
	public static int rpl = 1; //TODO *CHANGE HERE TO CHANGE RPL IF YOU WANT TO TRY OTHER POSSIBILITIES! WORKS FOR ALL! :D*
	public static Color gasColor = Color.GREEN;
	public static double addGasX = tankX + 1;
	public static double addGasY = tankY + tankHeight + 10;
	public static double addGasWidth = tankWidth;
	public static double addGasHeight = tankHeight/10.0;
	public static RectangularButton addGas = new RectangularButton((int)addGasX, (int)addGasY, (int)addGasWidth, (int)addGasHeight);
	CarFuelTank fuelTank;
	
	//Fuel meter's visual properties.
	public static double meterX = MainLauncher.controllerWidth/20.0;
	public static double meterY = MainLauncher.controllerWidth/1.9;
	public static double meterWidth = MainLauncher.controllerWidth/3.0;
	public static double meterHeight = MainLauncher.controllerWidth/25.0;
	public static Color meterColor = Color.RED;
	
	/**
	 * Paints the objects specified and created with the current data.
	 */
	public void paintComponent(Graphics g)
	{
		//Paints background of the interactive window.
		g.setColor(backgroundColor); 
		g.fillRect(0, 0, MainLauncher.controller.getWidth(), MainLauncher.controller.getHeight());
		
		//Draws developer's credentials.
		g.setColor(drawingColor);
		g.setFont(new Font("Monospaced", Font.BOLD, headerLetterSize - 5));
		g.drawString("Developed by Ivan J. Miranda.", (int)g.getClipBounds().getX(), (int)g.getClipBounds().getY() + 10);
		
		//Sets global drawing color.
		g.setColor(drawingColor);
		g.setFont(new Font("Monospaced", Font.BOLD, headerLetterSize));
		
		//Drawing the revolutions counter and its header.
		counter.draw(g, drawingColor);		
		g.drawString("Revolutions Counter", (int)revolutionsCounterX + 10, (int)revolutionsCounterY + 70);
		
		//Draws SpeedOMeter and its header (vertically and to the right).
		speedOMeter.draw(g, drawingColor, backgroundColor);	
		speedOMeterPointer = new CarGaugePointer((int)speedometerX, (int)speedometerY, 
						    (int)speedometerRadius, (int)(initialPointerPosition)
							);
		speedOMeterPointer.draw(g, pointerColor, drawingColor);
		for (int i = 0; i < SPEEDOMETER_HEADER_CHARACTER.length; i++)
		{	
			g.drawString(Character.toString(SPEEDOMETER_HEADER_CHARACTER[i]), (int)(speedometerX - 25), (int)(speedometerY + 25 + i*20));
		}
				
		//Drawing wheel and its header.
		wheel = new CarWheel((int)(wheelX), (int)(wheelY), (int)(wheelRadius), (int)wheelAngle); //Creates wheel.
		wheel.draw(g, drawingColor);
		for (int i = 0; i < WHEEL_HEADER_CHARACTER.length; i++)
		{	
			g.drawString(Character.toString(WHEEL_HEADER_CHARACTER[i]), (int)(wheelX + wheelDiameter + 15), (int)(wheelY + 85 + i*20));
		}
		
		//Drawing fuel tank, its meter, its header, its consumption specifications, and its "add gas" button.
		fuelTank = new CarFuelTank(tankX, tankY, tankWidth, tankHeight, tankCapacity, tankActualLoad);
		fuelTank.draw(g, drawingColor, gasColor);
		for (int i = 0; i < FUEL_TANK_HEADER_CHARACTER.length; i++)
		{	
			g.drawString(Character.toString(FUEL_TANK_HEADER_CHARACTER[i]), (int)(tankX + tankWidth + 15), (int)(tankY + 80 + i*20));
		}
		g.drawString("RPL: " + rpl, (int)(tankX - 10), (int)(tankY - 50));
		g.drawString("Max. Cap:", (int)(tankX - 20), (int)(tankY - 30));
		g.drawString(tankCapacity + "L", (int)(tankX + 15), (int)(tankY - 10));
		addGas.draw(g, drawingColor, buttonsColor);
		g.drawString("Add Gas", (int)(addGasX - 12), (int)(addGasY + addGasHeight + 20));
		
		//Drawing fuel meter, its labels, and its header.
		fuelTank.drawFuelMeter(g, drawingColor, meterColor, meterX, meterY, meterWidth, meterHeight);
		g.drawString("E", (int)meterX, (int)(meterY + meterHeight + 20));
		g.drawString("F", (int)(meterX + meterWidth - 12), (int)(meterY + meterHeight + 20));
		g.drawString("Fuel Meter", (int)(meterX + (meterWidth/4.0) + 3), (int)(meterY + meterHeight + 20));	
		
		//Draws speed controller, its labels, its header and its controlling buttons.
		speedOMeter.drawSpeedController(g, drawingColor, speedControllerIndicatorColor, 
								speedControllerX, speedControllerY, speedControllerWidth, speedControllerHeight
								);
		g.drawString("Off", (int)speedControllerX - 15, (int)(speedControllerY + speedControllerHeight + 20));
		g.drawString("Med", (int)(speedControllerX - 5 + speedControllerWidth/2 - 10), (int)(speedControllerY + speedControllerHeight + 20));
		g.drawString("Max", (int)(speedControllerX - 5 + speedControllerWidth - 10), (int)(speedControllerY + speedControllerHeight + 20));
		speedUp.draw(g, drawingColor, buttonsColor);
		speedDown.draw(g, drawingColor, buttonsColor);
		g.drawString("System Controllers:", (int)(meterX + 20), (int)(speedControllerY + speedControllerHeight + 80));
		g.drawString("Speed Up (+1 RPM)", (int)(speedControllerX + 75), (int)(speedControllerY + speedControllerHeight + 60));
		g.drawString("Brake (-1 RPM)", (int)(speedControllerX + 75), (int)(speedControllerY + speedControllerHeight + 100));
		
		
	} //End of paint method.
	
	/**
	 * *EXPERIMENTAL PHASE* This method updates the properties of the objects drawn as a function of window size. Useful for resizable windows.
	 * @param controllerWidth the window's width.
	 * @param controllerHeight the window's height.
	 */
	public static void updateSize(double controllerWidth, double controllerHeight) 
	{
		headerLetterSize = (int)(MainLauncher.controllerWidth/37.5);
		
		revolutionsCounterX = (controllerWidth/3.0) - 15;
		revolutionsCounterY = (controllerWidth/22.5);
		revolutionsCounterWidth = (controllerWidth/3.0);
		revolutionsCounterHeight = (controllerWidth/15.0);
		revolutionsCounterDigitsSize = (int)(MainLauncher.controllerWidth/12.5);
//		counter = new RevolutionsCounter((int)revolutionsCounterX, (int)revolutionsCounterY, //Revolutions counter resizing
//				(int)revolutionsCounterWidth, (int)revolutionsCounterHeight, 				 //doesn't work, yet, with animations.
//				REVOLUTIONS_COUNTER_POSITIONS, revolutionsCounterDigitsSize
//				);
		
		speedometerX = controllerWidth/20.0;
		speedometerY = controllerWidth/6.0;
		speedometerDiameter = controllerWidth/3.0;
		speedometerRadius = (speedometerDiameter)/2.0; 	
		speedometerDigitsSize = (int)(controllerWidth/37.5);
//		public static SpeedOMeter speedOMeter = new SpeedOMeter((int)(speedometerX), (int)(speedometerY), (int)(speedometerRadius), SPEEDOMETER_NUMBER_OF_MARKS, speedometerDigitsSize);
		//Speedometer resizing doesn't work, yet, with animations.
		speedControllerX = wheelX + 5;
		speedControllerY = wheelY + wheelDiameter + 20;
		speedControllerWidth = wheelDiameter;
		speedControllerHeight = wheelDiameter/8.5;
//		public static SpeedOMeter speedOMeter = new SpeedOMeter((int)(speedometerX), (int)(speedometerY), 
//														(int)(speedometerRadius), SPEEDOMETER_NUMBER_OF_MARKS, speedometerDigitsSize
//														);
//		public static RectangularButton speedUp = new RectangularButton((int)speedControllerX, (int)(speedControllerY + speedControllerHeight + 40),
//																		(int)(speedControllerWidth/4.0), (int)speedControllerHeight
//																		);
//		public static RectangularButton speedDown = new RectangularButton((int)speedControllerX, (int)(speedControllerY + speedControllerHeight + 80),
//																		  (int)(speedControllerWidth/4.0), (int)speedControllerHeight
//				                                                          );
		//DOES NOT WORK WITH BUTTONS, YET!
		
		wheelX = controllerWidth/2.5;
		wheelY = controllerWidth/6.0;
		wheelDiameter = controllerWidth/3.0;
		wheelRadius = wheelDiameter/2;
		
		tankX = MainLauncher.controllerWidth/1.175;
		tankY = MainLauncher.controllerWidth/7.0;
		tankWidth = MainLauncher.controllerWidth/12.5;
		tankHeight = MainLauncher.controllerWidth/2.5;
		addGasX = tankX + 1;
		addGasY = tankY + tankHeight + 10;
		addGasWidth = tankWidth;
		addGasHeight = tankHeight/10.0;
		//public static RectangularButton addGas = new RectangularButton((int)tankX + 1, (int)(tankY + tankHeight + 10), (int)tankWidth, (int)(tankHeight/10.0));
		//Buttons resizing doesn't work, yet, with animations.
		
		meterX = MainLauncher.controllerWidth/20.0;
		meterY = MainLauncher.controllerWidth/1.9;
		meterWidth = MainLauncher.controllerWidth/3.0;
		meterHeight = MainLauncher.controllerWidth/25.0;
	}
	
} //End of class.
