import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

/**
 * This class runs the main method and simulates a car's performance in a interactive way.
 * @author Ivan J. Miranda
 *
 */

public class MainLauncher implements MouseListener
{
	//The controller is the interactive window this program uses to perform correctly. Controller's visual properties are declared here.
	static JFrame controller;
	Interface controllerInterface;
	public static int controllerWidth = 750, controllerHeight = 600;
	public static int controllerLocationX = 100, controllerLocationY = 75;
	public static boolean windowIsVisible = true;
	
	//The following are variables declared for animation control.
	public static final int FRAME_SPEED = 100; //in milliseconds.
	public static boolean addGasIsPressed = false;
	public static boolean speedUpIsPressed = false;
	public static boolean speedDownIsPressed = false;
	public static boolean literTaken = false;
	public static int counterData; //These last two are used to check if counter's data has changed in order to not decrease gas level
	public static int lastCounterData; //on each frame the rev. counter reads the same data that triggered it.
	
	//Main method launches the program.
	public static void main(String[] args) 
	{
		new MainLauncher().launch();
	} //End of main method.
	
	/**
	 * Launch method contains the instructions that will prepare the interactive window.
	 */
	private void launch()
	{
		controller = new JFrame("Car Perfomance Simulator");
		controller.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		controllerInterface = new Interface(); //Class Interface contains all visual components related to the program.
		
		controller.getContentPane().add(BorderLayout.CENTER, controllerInterface);
		controller.setResizable(false); //False for the moment. The visual properties are based on window's size ratios for future resizable features.
									   //Refer to Interface class' initial object's properties and updateSize method.
		controller.setSize(controllerWidth, controllerHeight);
		controller.setLocation(controllerLocationX, controllerLocationY);
		controller.setVisible(windowIsVisible);
		
		update();
		
	} //End of launch method.
	
	/**
	 * This method updates the graphical properties of the objects drawn in the Graphics instance being used.
	 */
	private void update()
	{
		controller.addMouseListener(this);
		
		counterData = Integer.parseInt(Interface.counter.toString());
		lastCounterData = counterData; //intializes both, and both are equal.
		
		while (windowIsVisible)
		{
			//The following three commented lines are instructions that are to be enabled when resizing dependency works properly.
//				controllerWidth = controller.getWidth();
//				controllerHeight = controller.getHeight();
//				controllerInterface.updateSize(controllerWidth, controllerHeight);
			
			//Synchronizes speed with the revolutions that the wheel makes, taking into account frame speed. (Thread.sleep time)
			Interface.wheelAngle += (360.0*(((double)(Interface.speedOMeter.getSpeed())/60.0/1000.0)*FRAME_SPEED));
			
			//Checks wether a revolution has been completed by the wheel and adds one to the counter.
			if (Interface.wheelAngle > Interface.startingAngle + 360 && Interface.speedOMeter.getSpeed() > 0)
			{
				Interface.counter.addRevolution();
				Interface.wheelAngle -= 360;
			}
			
			counterData = Integer.parseInt(Interface.counter.toString()); //Retrieves and updates counter's data
			
			if (counterData != lastCounterData) //Checks if there has been any change from last time gas was decreased.
				literTaken = false;
						
			//System runs if you have gas in the tank!
			if (Interface.tankActualLoad > 0)
			{	//Gas decrease depends on RPL. If counterData is a multiple of tank's RPL, it then decreases gas by one liter.		
				if (counterData % Interface.rpl == 0 && counterData != 0 && Interface.speedOMeter.getSpeed() > 0 && !literTaken)
				{
					Interface.tankActualLoad--;
					literTaken = true; //LiterTaken boolean prevents the reduction of gas on each frame the revolutions counter reads the same 
					lastCounterData = counterData; //data that triggered it. It changes back to false when these last two are no longer equal.
				}
			}
			else //If you ran out of gas, system slows down steadily.
			{
				if (Interface.speedOMeter.getSpeed() > 0)
					Interface.speedOMeter.decreaseSpeed();
			}
			
			//Checks if addGas button is pressed, and if the tank's not full, and executes instruction if true.
			if (addGasIsPressed && Interface.tankActualLoad < Interface.tankCapacity)
				Interface.tankActualLoad++; //Increases by one liter.
			
			if (speedUpIsPressed && Interface.tankActualLoad > 0 && Interface.speedOMeter.getSpeed() < Interface.speedOMeter.getMaxSpeed())
				Interface.speedOMeter.increaseSpeed(); //Increases RPM by 1.
			
			if (speedDownIsPressed && Interface.speedOMeter.getSpeed() > 0)
				Interface.speedOMeter.decreaseSpeed(); //Decreases RPM by 1.
			
			//The following instruction updates the SpeedOMeter's pointer initial position.
			Interface.initialPointerPosition = (3*360.0/Interface.SPEEDOMETER_NUMBER_OF_MARKS) + 
											    Interface.speedOMeter.getSpeed()*Interface.speedOMeter.getAnglePerRevolution();
		
			controller.repaint(); //Animates!
			
			try {
				Thread.sleep(FRAME_SPEED);
			} catch (InterruptedException e) {
				//Catches any interruption so the program does not crashes.
				e.printStackTrace();
			}
			
		}
	}

	/**
	 * Registers a click and does nothing about it.
	 * @param arg0 the respective mouse event. It is sent automatically by the system.
	 */
	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		//Nothing is done with the mouse press/release combination.
		
	}
	
	/**
	 * Registers mouse pointer entering frame where mouse listener was registered and does nothing about it.
	 * @param arg0 the respective mouse event. It is sent automatically by the system.
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) 
	{
		//Nothing is done with the "mouse entering the frame" event.
	}

	
	/**
	 * Registers mouse pointer exiting frame where mouse listener was registered and does nothing about it.
	 * @param arg0 the respective mouse event. It is sent automatically by the system.
	 */
	@Override
	public void mouseExited(MouseEvent arg0) 
	{
		//Nothing is done with the "mouse exiting the frame" event.
		
	}

	
	/**
	 * Registers mouse press in frame where mouse listener was registered, checks whether any of the frame's buttons where pressed
	 * and, if any was pressed, it executes the pressed button's function.
	 * @param arg0 the respective mouse event. It is sent automatically by the system.
	 */
	@Override
	public void mousePressed(MouseEvent arg0)
	{
		//Checks if mouse press was on addGas button's location and executes animation along with changing respective boolean to true if it is.
		if (arg0.getX() >= Interface.addGas.getInnerX() + 3 && arg0.getX() < (Interface.addGas.getInnerX() + Interface.addGas.getInnerWidth() + 3))
			if (arg0.getY() >= Interface.addGas.getInnerY() + 25 && arg0.getY() < (Interface.addGas.getInnerY() + Interface.addGas.getInnerHeight() + 25))
			{	
				addGasIsPressed = true;
				Interface.addGas.pressed();
			}
		//Checks if mouse press was on speedUp button's location and executes animation along with changing respective boolean to true if it is.
		if (arg0.getX() >= Interface.speedUp.getInnerX() + 3 && arg0.getX() < (Interface.speedUp.getInnerX() + Interface.speedUp.getInnerWidth() + 3))
			if (arg0.getY() >= Interface.speedUp.getInnerY() + 25 && arg0.getY() < (Interface.speedUp.getInnerY() + Interface.speedUp.getInnerHeight() + 25))
			{	
				speedUpIsPressed = true;
				Interface.speedUp.pressed();
			}
		
		//Checks if mouse press was on speedDown button's location and executes animation along with changing respective boolean to true if it is.
		if (arg0.getX() >= Interface.speedDown.getInnerX() + 3 && arg0.getX() < (Interface.speedDown.getInnerX() + Interface.speedDown.getInnerWidth() + 3))
			if (arg0.getY() >= Interface.speedDown.getInnerY() + 25 && arg0.getY() < (Interface.speedDown.getInnerY() + Interface.speedDown.getInnerHeight() + 25))
			{	
				speedDownIsPressed = true;
				Interface.speedDown.pressed();
			}
	}


	/**
	 * Registers mouse release in frame where mouse listener was registered. Returns all buttons to their released state.
	 * @param arg0 the respective mouse event. It is sent automatically by the system.
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) 
	{
		//All buttons returns to their original state if mouse is released... logically.
		addGasIsPressed = false;
		Interface.addGas.released();
		speedUpIsPressed = false;
		Interface.speedUp.released();
		speedDownIsPressed = false;
		Interface.speedDown.released();
	}
	
} //End of class.

