import java.awt.Graphics;
import java.awt.Color;

/**
 * Objects of this class represent a button and its states.
 * @author Ivan J. Miranda Marrero
 *
 */
public class RectangularButton 
{
	private int xLocation; //The x coordinate of the button (top-left)
	private int yLocation; //The y coordinate of the button (top-left).
	private int width; //The button's width.
	private int height; //The button's height.
	private int innerLocationX; //The animation rectangle's x.
	private int innerLocationY; //The animation rectangle's y.
	private int innerWidth; //The animation rectangle's width.
	private int innerHeight; //The animation rectangle's height.
	
	/**
	 * Creates a new rectangular button with the properties given.
	 * @param xLocation the x coordinate of the button (top-left).
	 * @param yLocation the y coordinate of the button (top-left).
	 * @param width the button's width.
	 * @param height the button's height.
	 */
	public RectangularButton(int xLocation, int yLocation, int width, int height)
	{
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.width = width;
		this.height = height;
		innerLocationX = xLocation;
		innerLocationY = yLocation;
		innerWidth = width - 2;
		innerHeight = height - 2;
	}
	
	/**
	 * Returns the button's x coordinate (top-left).
	 * @return the button's x coordinate.
	 */
	public int getX()
	{
		return xLocation;
	}
	
	/**
	 * Returns the button's y coordinate (top-left).
	 * @return the button's y coordinate.
	 */
	public int getY()
	{
		return yLocation;
	}
	
	/**
	 * Returns the button's width.
	 * @return the button's width.
	 */
	public int getWidth()
	{
		return width;
	}
	
	/**
	 * Returns the button's height.
	 * @return the button's height.
	 */
	public int getHeight()
	{
		return height;
	}
	
	/**
	 * Returns the button's inner rectangle's x coordinate (top-left).
	 * @return the button's inner rectangle's x coordinate.
	 */
	public int getInnerX()
	{
		return innerLocationX;
	}
	
	/**
	 * Returns the button's inner rectangle's y coordinate (top-left).
	 * @return the button's inner rectangle's y coordinate.
	 */
	public int getInnerY()
	{
		return innerLocationY;
	}
	
	/**
	 * Returns the button's inner rectangle's width.
	 * @return the button's inner rectangle's width.
	 */
	public int getInnerWidth()
	{
		return innerWidth;
	}
	
	/**
	 * Returns the button's inner rectangle's height.
	 * @return the button's inner rectangle's height.
	 */
	public int getInnerHeight()
	{
		return innerHeight;
	}
	
	/**
	 * Draws the current's instance of this button.
	 * @param g the Graphics instance in which it will be drawn.
	 * @param c the color in which it will be drawn.
	 * @param borderColor the button's border color.
	 */
	public void draw(Graphics g, Color c, Color borderColor)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		
		g.setColor(borderColor);
		g.drawRect(xLocation, yLocation, width, height);
		
		g.setColor(c);
		g.fillRect(innerLocationX, innerLocationY, innerWidth, innerHeight);
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
	}
	
	/**
	 * Changes the button's visual state to pressed.
	 */
	public void pressed()
	{
		innerLocationX = xLocation + 1;
		innerLocationY = yLocation + 1;
	}
	
	/**
	 * Changes the button's visual state to released.
	 */
	public void released()
	{
		innerLocationX = xLocation;
		innerLocationY = yLocation;
	}
}
