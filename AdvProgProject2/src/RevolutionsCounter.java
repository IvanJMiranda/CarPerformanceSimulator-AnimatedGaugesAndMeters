import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 * Objects of this type represent a car's revolutions counter.
 * @author Ivan J. Miranda Marrero
 *
 */
public class RevolutionsCounter
{
	private final int POSITIONS; //The number of digits the counter will have.
	private int[] digit; //The counter's data.
	private int xLocation; //The x coordinate of the counter.
	private int yLocation; //The y coordinate of the counter.
	private int width; //The width of the counter.
	private int height; //The height of the counter.
	private int digitsSize; //The size of the counter's digits.
	
	/**
	 * Creates a new revolutions counter with the properties specified, with no revolutions counted yet.
	 * @param xLocation the x coordinate of the counter.
	 * @param yLocation the y coordinate of the counter.
	 * @param width the width of the counter.
	 * @param height the height of the counter.
	 * @param POSITIONS the number of digits the counter will have.
	 * @param digitsSize the size of the counter's digits.
	 */
	public RevolutionsCounter(int xLocation, int yLocation, int width, int height, int POSITIONS, int digitsSize)
	{
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.width = width;
		this.height = height;
		this.digitsSize = digitsSize;
		this.POSITIONS = POSITIONS;
		this.digit = new int [POSITIONS];
		for (int i = 0; i < POSITIONS; i++)
			{
			digit[i] = 0;
			}
	}
	
	/**
	 * Draws the current instance of this revolutions counter with the given properties.
	 * @param g the Graphics instance in which this object will be drawn.
	 * @param c the color in which it will be drawn.
	 */
	public void draw(Graphics g, Color c)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		Font originalFont = g.getFont(); //Saves original font in the Graphics instance.
		
		g.setColor(c);
		g.drawRect(xLocation, yLocation, width, height);
		
		g.setFont(new Font("Monospaced", Font.PLAIN, digitsSize)); //Font properties (Name, style, size)
		g.drawChars(this.toString().toCharArray(), 0, digit.length, xLocation, yLocation + 45); //End of digits' drawing.
		
		for(int i = 0; i < POSITIONS; i++) //Draws divisions between each digit forming equally sized positions.
		{
			g.drawLine( //Receives two points that will indicate between where to draw a line.
					    (int)(xLocation + i*((float)width/POSITIONS)), //x1
					    (int)yLocation, //y1
					    (int)(xLocation + i*((float)width/POSITIONS)), //x2
					    (int)(yLocation + height) //y2
					  );
		} //End of iteration.
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
		g.setFont(originalFont); //Sets back the font to the original font the Graphics instance had before using this method.
	}
	
	/**
	 * Adds one revolution to the revolution's counter of this object.
	 */
	public void addRevolution()
	{
		boolean revolutionAdded = false;
		for (int i = POSITIONS - 1; i >= 0 && !revolutionAdded; i--)
		{
			if (digit[i] < 9)
			{
				digit[i]++;
				revolutionAdded = true;
			}
			else
			{
				digit[i] = 0;
				if (i == 0 && !revolutionAdded)
					reset();
			}
		}			
	}
	
	/**
	 * Resets the counter's data to zero.
	 */
	public void reset()
	{
		for (int i = 0; i < POSITIONS; i++)
			digit[i] = 0;
	}
	
	/**
	 * Converts the counter's data to a string of consecutive characters where each character equals the corresponding digit in the counter's data.
	 * @return the String that represents the counter's data.
	 */
	public String toString()
	{
		String digitString = "";
		for (int i = 0; i < POSITIONS; i++)
			digitString += digit[i];
		return digitString;			
	}
}
